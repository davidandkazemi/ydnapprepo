# README


Hello, we are David Townley JE '20 and Kazemi Adachi JE '20 and this is our CS50 final project -- the __*YDN App*__.*

---

To begin, we are using XCode version 9.1 for all of our coding, which is running on Swift 4. Once the application code has been downloaded, the user must download cocoapods by moving to the directory with the code using `cd <directory/location/containing/YDN App>` and then running the command `sudo gem install cocoapods` in the terminal. Once cocopods has been installed, the user must then type `pod install` into the terminal so that the `RealmSwift` cocoapod is downloaded. After this is done, the user should go to the terminal and open the `YDN App.xcworkspace` in order to configure the application.

For the first initialization, uncomment lines 65 to 77 in `ArticleTableViewController.swift` in order to download and parse the XML data for the Yale Daily News.** This will download the 10 most recent articles in each of the categories (more on this below).

At this point, running the application is as easy as clicking the `Run` icon (represented by a play button) in the top left of the XCode terminal. Though there may initially be an error reading that RealmSwift is not recognized, this will go away once the Swift source files are compiled. On the first compilation, the entire project needs to be indexed and processed, and 78 Realm source files will have to be downloaded, so this initialization may take some time.

Eventually, the app will run, and it will open in an iPhone simulator. At the moment, we only support iPhones, and do not support iPads for our application. The simulator will show our loading screen, and then _voila_, the __YDN App__.

Upon opening the app, the user is first confronted with our main page, or what we like to call the _Front Page_. The Front Page contains all of the articles loaded into the Realm Database (See: `DESIGN.md` for more details) sorted by most recent. When any article is clicked, the interface changes to that article's view, and the article content is displayed. In addition, if the YDN published the article with a video or with an image, then that media content is also displayed. More on videos later. In addition, in the top right corner is an icon for _sharing_. Clicking on that button, the user can configure his/her own sharing settings so that the user can very easily share via messenger, Facebook, Twitter ... you name it!

Returning to the main feed via the `Front` button in the top left corner, the user can scroll down to discover more articles. In addition, the user the can scroll all the way up to discover the _search bar_. The search bar works just as any ole' search bar would be expected to work, with the additional functionality of being able to filter results by the articles' titles, authors, and contents.

There is one more major functionality to our app, and that is _Categories_. Clicking on the "hamburger" icon in the top left of the main screen, causes a side menu to slide in. According to the YDN, there are 9 different categories in their newspaper, and they are "Opinion", "University", "City", "Sports", "SciTech", "Culture", "YTV", "Weekend", and "Magazine". Clicking on any one of them reloads the view so that only the articles that pertain to that category are displayed! Once more, the search functionality exists, with the additional filter that it will only search for articles within that category.

Let's now click on the "YTV" category and open the first article at the moment, "The Expansion of the Art Gallery". After giving the page a second or two to buffer, the Youtube video magically appears! Clicking on the video, it begins to play, and we can easily learn more about the arts at Yale.

Thank you for reading about our YDN App!

--

Copyright 2017 David Townley and Kazemi Adachi

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "YDN App"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

---

*All content shown on this app is the property and copyright of the Yale Daily News. We do not claim any ownership over the content nor the images and icons associated with the Yale Daily News shown in this app.

**Known bugs:
* Adding articles to the database by uncommenting the `fetchData(source)` block in `ArticleTableViewController.swift` is known to return NSXMLParserErrors. To maintain stability, we recommend only fetching the data once, and then recommenting this block. While this eliminates the ability to update the feed, stored articles will remain stored, and this process can mostly work when repeated 24 hours later (because the YDN has published new articles).
* Occasionally an article will load but without the content text, for example 'A Work of Artism' on the current main page. While simple debugging tells us that this property is definitely stored in the Article Object, it is not being displayed from time to time, so we are working on fixing this.
* There are some HTML text entries such as `&#nbsp;` that we are working on resolving to replace with their `UTF-8` counterparts.
* There may be extra space at the end of a document. More than is supposed to be there.
* Twice rotating with the search bar open can cause the formatting to become screwed up.
