//
//  ArticleViewController.swift
//  YDN App
//
//  Created by David and Kazemi on 12/10/17.
//  Copyright © 2017 David Townley, Kazemi Adachi. All rights reserved.
//

import UIKit

class ArticleViewController: UIViewController {
    
    //MARK: Properties
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var imageCreditLabel: UILabel!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var backButton: UIBarButtonItem!
    
    var article: Article!
    var imageViewHeight: CGFloat!
    var imageViewWidth: CGFloat!
    var aspectRatio: CGFloat!
    
    // prepare the view controller for initial presentation
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // check if articles was passed in segue
        if let article = article {
            
            // format page
            titleLabel.text = article.title
            
            authorLabel.text = "by " + article.author.uppercased()
            
            dateLabel.text = formatDate(pubDate: article.pubDate)
            
            imageCreditLabel.text = article.imageCredit
            
            // Format the article content text
            formatContent()

            // Prepare the image
            getImage(url: article.imageLink)
        }
        else {
            fatalError("Article was not passed through to ArticleViewController")
        }
    }
    
    
    // If the user rotates the screen, adjust the dimensions of the image view
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: {_ in
            self.imageHeightConstraint.constant = self.articleImageView.frame.size.width * self.aspectRatio
        })
    }
    
    
    // nitty gritty of formatting the attributes of the content string
    func formatContent () {
        // https://stackoverflow.com/questions/39158604/how-to-increase-line-spacing-in-uilabel-in-swift
        let attributedString = NSMutableAttributedString(string: article.content)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 3
        paragraphStyle.paragraphSpacingBefore = 16
        attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        contentLabel.attributedText = attributedString
        contentLabel.sizeToFit()
    }

    
    // getting an image from the url parsed from the ydn website
    func getImage(url: String) {
        if let url = URL(string: url) {
            let session = URLSession.shared
            session.dataTask(with: url) { (data, response, error) in
                if let data = data {
                    // set the image
                    let image = UIImage(data: data)
                    DispatchQueue.main.async() {
                    self.articleImageView.image = image
                    // resize the image
                    self.aspectRatio = (image?.size.height)! / (image?.size.width)!
                    self.imageHeightConstraint.constant = self.articleImageView.frame.size.width * self.aspectRatio
                    }
                }
            }.resume()
        }
        
        // delete image view if no image
        if url == "" {
            self.articleImageView.removeFromSuperview()
            print("deleted")
        }
    }
    
    
    // Dismiss the articleViewController if the user hits the 'back' button
    @IBAction func back(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    
    // present an activity view controller if the user hits the share button
    @IBAction func shareButtonClicked(sender: UIButton) {
        
        // send the title along with the link to recipients of the share
        let textToShare = self.article.title + ":"
        
        if let website = NSURL(string: self.article.link) {
            let objectsToShare = [textToShare, website] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            activityVC.popoverPresentationController?.sourceView = sender
            self.present(activityVC, animated: true, completion: nil)
        }
    }
}

