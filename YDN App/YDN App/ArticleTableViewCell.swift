//
//  ArticleTableViewCell.swift
//  YDN App
//
//  Created by David and Kazemi on 12/10/17.
//  Copyright © 2017 David Townley, Kazemi Adachi. All rights reserved.
//

import UIKit

class ArticleTableViewCell: UITableViewCell {
    
    // MARK: Properties
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    
    
    // Formats the cell when given an article
    var item: Article! {
        didSet {
            titleLabel.text = item.title
            descriptionLabel.text = item.description_
            authorLabel.text = item.author.capitalized
            
            // formats the date properly as a string
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "E, d MMM yyyy h:mm a"
            var localTimeZoneAbbreviation: String { return TimeZone.current.abbreviation() ?? "" }
            dateFormatter.timeZone = TimeZone(abbreviation: localTimeZoneAbbreviation)
            let timeInterval = Double(item.pubDate)
            let date = Date(timeIntervalSince1970: timeInterval)
            dateLabel.text = dateFormatter.string(from: date)
            
           
        }
    }
}

