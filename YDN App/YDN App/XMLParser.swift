//
//  XMLParser.swift
//  YDN App
//
//  Created by David and Kazemi on 12/10/17.
//  Copyright © 2017 David Townley, Kazemi Adachi. All rights reserved.
//
// source: Duc Tran, Developer's Academy https://www.youtube.com/watch?v=fP69LI5bZlg

import Foundation
import RealmSwift

// Article class for storing data, some of which we did not use but has very little storage costs such as "comments"
class Article: Object {
    @objc dynamic var title = ""
    @objc dynamic var link = ""
    @objc dynamic var comments = ""
    @objc dynamic var pubDate = Int()
    @objc dynamic var author = ""
    @objc dynamic var category = ""
    @objc dynamic var description_ = ""
    @objc dynamic var content = ""
    
    @objc dynamic var imageLink = ""
    @objc dynamic var imageCredit = ""
    
    @objc dynamic var videoLink = ""
    
    override static func primaryKey() -> String? {
        return "pubDate"
    }
}

// download XML from a server using XMLParser
// parse xml to foundation objects
// call back


class FeedParser: NSObject, XMLParserDelegate {
    
    //MARK: Properties
    
    private var articles: [Article] = []
    private var currentElement = ""
    
    // initializes several private variables which are strings to represent what is between the XML tags
    // when each current field is first set, the didSet(...) cleans it up so that it looks nice
    private var currentTitle: String = "" {
        didSet {
            currentTitle = currentTitle.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
    }
    private var currentLink: String = "" {
        didSet {
            currentLink = currentLink.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
    }
    private var currentComments: String = "" {
        didSet {
            currentComments = currentComments.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
    }
    private var currentPubDate: String = "" {
        didSet {
            currentPubDate = currentPubDate.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
    }
    private var currentAuthor: String = "" {
        didSet {
            currentAuthor = currentAuthor.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
    }
    private var currentDescription: String = "" {
        didSet {
            currentDescription = currentDescription.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            currentDescription = currentDescription.replacingOccurrences(of: "[&#8230;]", with: "...")
            currentDescription = currentDescription.replacingOccurrences(of: "&#8217;", with: "'")
            currentDescription = currentDescription.replacingOccurrences(of: "&#8220;", with: "\"")
            currentDescription = currentDescription.replacingOccurrences(of: "&#8221;", with: "\"")
            currentDescription = currentDescription.replacingOccurrences(of: "&#160; ", with: "")
        }
    }
    private var currentCategory: String = "" {
        didSet {
            currentCategory = currentCategory.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
    }
    private var currentContent: String = "" {
        didSet {
            currentContent = currentContent.trimmingCharacters(in: CharacterSet.whitespaces)
            currentContent = currentContent.replacingOccurrences(of: "<[^>]+>", with: "", options: String.CompareOptions.regularExpression, range: nil)
            currentContent = currentContent.replacingOccurrences(of: "&#8217;", with: "'")
            currentContent = currentContent.replacingOccurrences(of: "&#8220;", with: "\"")
            currentContent = currentContent.replacingOccurrences(of: "&#8221;", with: "\"")
            currentContent = currentContent.replacingOccurrences(of: "&nbsp;", with: "\n")
            currentContent = currentContent.replacingOccurrences(of: "&#8211;", with: "--")
            currentContent = currentContent.replacingOccurrences(of: "&amp;", with: "&")
        }
    }
    private var currentImageCredit: String = "" {
        didSet {
            currentImageCredit = currentImageCredit.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
    }
    
    private var parserCompletionHandler: (([Article]) -> Void)?
    
    
    //MARK: Methods
    
    func parseFeed(url: String, completionHandler: (([Article]) -> Void)?) {
        self.parserCompletionHandler = completionHandler
        
        // gets the xml data from the URL search
        let request = URLRequest(url: URL(string: url)!)
        let urlSession = URLSession.shared
        let task = urlSession.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                if let error = error {
                    print(error.localizedDescription)
                }
                
                return
            }
            
            // parse our xml data
            let parser = XMLParser(data: data)
            parser.delegate = self
            parser.parse()
        }
        
        task.resume()
    }
    
    //MARK: - XML Parser Delegate
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        currentElement = elementName
        if currentElement == "item" {
            currentTitle = ""
            currentLink = ""
            currentComments = ""
            currentPubDate = ""
            currentAuthor = ""
            currentCategory = ""
            currentDescription = ""
            currentContent = ""
        }
    }

    // updates the found characters within an item, depending on the closing tag of each xml instance
    internal func parser(_ parser: XMLParser, foundCharacters string: String) {
        switch currentElement {
        case "title": currentTitle += string
        case "link": currentLink += string
        case "comments": currentComments += string
        case "pubDate": currentPubDate += string
        case "dc:creator": currentAuthor += string
        case "category": currentCategory += string
        case "description": currentDescription += string
        case "content:encoded": currentContent += string
        default: break
        }
    }
    
    // the YDN often puts multiple non-main categories onto strings, so this only yields the main category
    internal func categorySimplifier(string: String) -> String {
        if string.lowercased().range(of:"opinion") != nil {
            return "Opinion"
        } else if string.lowercased().range(of:"university") != nil {
            return "University"
        } else if string.lowercased().range(of:"city") != nil {
            return "City"
        } else if string.lowercased().range(of:"sports") != nil {
            return "Sports"
        } else if string.lowercased().range(of:"sci-tech") != nil {
            return "SciTech"
        } else if string.lowercased().range(of:"culture") != nil {
            return "Culture"
        } else if string.lowercased().range(of:"ytv") != nil {
            return "YTV"
        } else if string.lowercased().range(of:"weekend") != nil {
            return "Weekend"
        } else if string.lowercased().range(of:"magazine") != nil {
            return "Magazine"
        } else {
            return ""
        }
    }
    
    internal func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "item" {
            
            // populates the properties of a new article class instance
            
            let article = Article()
            article.title = currentTitle
            article.link = currentLink
            article.comments = currentComments
            
            // transforms the pubDate String into an Int for storage and sorting
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "E, d MMM yyyy HH:mm:ss Z"
            let timeInterval = dateFormatter.date(from: currentPubDate)?.timeIntervalSince1970
            article.pubDate = Int(timeInterval!)
            
            article.author = currentAuthor
            article.category = categorySimplifier(string: currentCategory)
            article.description_ = currentDescription
            article.content = currentContent
            
            // does regex searches on the view-source url of the link to find the youtube video (if applicable), or the image and image credit (if applicable)
            do {
                let HTMLString = try String(contentsOf: URL(string: currentLink)!, encoding: .ascii)
                let length = HTMLString.count
                
                let regex = try! NSRegularExpression(pattern: "(?<=class=\\'card-image\\' src=\\')[^\\'>]*", options: [])
                let imageMatches = regex.matches(in: HTMLString, range: NSRange(location: 0, length: length))
                if(imageMatches.count > 0) {
                    let imageRange = Range(imageMatches[0].range, in: HTMLString)!
                    article.imageLink = String(HTMLString[imageRange])
                }
                let creditRegex = try! NSRegularExpression(pattern: "(?<=image-link\\\" href=\\\")([^<]*)", options: [])
                let creditMatches = creditRegex.matches(in: HTMLString, range: NSRange(location: 0, length: length))
                if(creditMatches.count > 0) {
                    let creditRange = Range(creditMatches[0].range, in: HTMLString)!
                    let credit_0 = String(HTMLString[creditRange])
                    let creditRegex1 = try! NSRegularExpression(pattern: "(?<=>)(?:[^>]*)", options: [])
                    let creditMatches1 = creditRegex1.matches(in: credit_0, range: NSRange(location: 0, length: credit_0.count))
                    let creditRange1 = Range(creditMatches1[0].range, in: credit_0)!
                    if creditRange1.lowerBound != creditRange1.upperBound {
                        currentImageCredit = String(credit_0[creditRange1])
                    }
                    article.imageCredit = currentImageCredit
                }
                let videoRegex = try! NSRegularExpression(pattern: "(?<=class=\\\"ytv-article-iframe item\\\" src= )[^' ]*", options: [])
                let videoMatches = videoRegex.matches(in: HTMLString,range: NSRange(location: 0, length: length))
                if(videoMatches.count > 0) {
                    let videoRange = Range(videoMatches[0].range, in: HTMLString)!
                    article.videoLink = String(HTMLString[videoRange])
                }
            } catch let error {
                print("Error: \(error)")
            }
            
            // finally, adds the new article instance to an array of articles
            self.articles.append(article)
        }
    }
    
    internal func parserDidEndDocument(_ parser: XMLParser) {
        parserCompletionHandler?(articles)
    }
    
    internal func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        print(parseError.localizedDescription)
    }
}

