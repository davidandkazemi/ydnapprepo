//
//  MainContainerViewController.swift
//  YDN App
//
//  Created by David and Kazemi on 12/10/17.
//  Copyright © 2017 David Townley, Kazemi Adachi. All rights reserved.
//

import UIKit

class MainContainerViewController: UIViewController {

    // MARK: Properties
    @IBOutlet weak var sideBarContainer: UIView!
    @IBOutlet weak var sideMenuConstraint: NSLayoutConstraint!
    @IBOutlet weak var feedContainer: UIView!
    
    var sideMenuOpen = false
    
    // MARK: Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // add an observer for notifications indicating that the view controller should toggle the side menu
        NotificationCenter.default.addObserver(self, selector: #selector(toggleSideMenu), name: NSNotification.Name("ToggleSideMenu"), object: nil)
        
        // allow the view controller to recognize when a user swipes left so that it can run the proper function
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(MainContainerViewController.swipedLeft))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
        
        // allow the view controller to recognize when a user swipes right so that it can run the proper function
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(MainContainerViewController.swipedRight))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        // drop a shadow on the feed container for aesthetique purposes when the side menu is out
        feedContainer.dropShadow()
    }
    
    // when the side menu is out, slides the menu back in when the user touches outside of the side menu
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch? = touches.first
        // if the touch is not in the side menu and the side menu is open, notify the controller to close the menu
        if touch?.view != sideBarContainer && sideMenuOpen == true {
            NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
        }
    }
    
    // funciton that closes the side menu
    @objc func toggleSideMenu() {
        // if the menu is open, close it with the constraint and indicate that it is closed
        if sideMenuOpen {
            sideMenuConstraint.constant = -258
            sideMenuOpen = false
            // then, re-enable the feed
            NotificationCenter.default.post(name: NSNotification.Name("ToggleEnableMainView"), object: nil)
        }
        // otherwise, open the menu using the constraint, and disable the main feed
        else {
            sideMenuConstraint.constant = 0
            sideMenuOpen = true
            NotificationCenter.default.post(name: NSNotification.Name("ToggleEnableMainView"), object: nil)
        }
        // animate the transition
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseIn, animations: {
            self.view.layoutIfNeeded()
        })
        
    }
    
    // function that closes the side menu if the side menu is open, by swiping left
    @objc func swipedLeft() {
        if sideMenuOpen {
            NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
        }
    }
    
    // function that opens the side menu if the side menu is closed, by swiping right
    @objc func swipedRight() {
        if !sideMenuOpen {
            NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
        }
    }
}



