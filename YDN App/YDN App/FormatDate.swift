//
//  FormatDate.swift
//  YDN App
//
//  Created by David and Kazemi on 12/10/17.
//  Copyright © 2017 David Townley, Kazemi Adachi. All rights reserved.
//

import Foundation

// nitty gritty of formatting the date
// converts a date integer (in unix epoch time sinze 1970) to XCode NSDate class in the user's local timezone
func formatDate (pubDate: Int) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "E, d MMM yyyy h:mm a"
    var localTimeZoneAbbreviation: String { return TimeZone.current.abbreviation() ?? "" }
    dateFormatter.timeZone = TimeZone(abbreviation: localTimeZoneAbbreviation)
    let timeInterval = Double(pubDate)
    let date = Date(timeIntervalSince1970: timeInterval)
    return dateFormatter.string(from: date)
}
