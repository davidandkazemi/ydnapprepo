//
//  Content.swift
//  YDN App
//
//  Created by David and Kazemi on 12/10/17.
//  Copyright © 2017 David Townley, Kazemi Adachi. All rights reserved.
//

import UIKit

// define the category class
class Category {
    var image: UIImage?
    var name: String
    
    init(image: UIImage?, name: String) {
        // Initialize stored properties.
        self.image = image
        self.name = name
    }
}
