//
//  SideBarTableViewController.swift
//  YDN App
//
//  Created by David and Kazemi on 12/10/17.
//  Copyright © 2017 David Townley, Kazemi Adachi. All rights reserved.
//

import UIKit

class SideBarTableViewController: UITableViewController {

    // Mark: Properties
    var recentlySelectedCategory: Category!
    var categories = [Category]()
    
    // prepare the view controller
    override func viewDidLoad() {
        super.viewDidLoad()

        // load the different categories
        loadCategories()
    }

    // MARK: - Table view data source
    
    // Set the number of sections to 1
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // set the number of rows to the number of categories
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return categories.count
    }
    
    // configure the cells
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "SideBarCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? SideBarTableViewCell  else {
            fatalError("The dequeued cell is not an instance of SideBarTableViewCell.")
        }
        
        // Fetches the appropriate category for the data source layout.
        let category = categories[indexPath.row]
        
        cell.category = category
        
        return cell
    }
    
    // define behavior when a cell is selected
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // getting the index path of selected row
        let indexPath = tableView.indexPathForSelectedRow
        
        // getting the cell
        guard let currentCell = tableView.cellForRow(at: indexPath!) as? SideBarTableViewCell else {
            fatalError("Unexpected cell type, not ArticleTableViewCell")
        }
        
        // configure the information about the category into a dictionary so that it can be sent through objective C
        let categoryDataDict:[String: Category] = ["category": currentCell.category]
        
        // notify the Article Table View Controller to change its category
        NotificationCenter.default.post(name: NSNotification.Name("ChangeCategory"), object: nil, userInfo: categoryDataDict)
        
        // notify the Main Container View Controller to toggle the side menu back to closed
        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
    }
    
    
    //MARK: Private Methods
    
    // load the categories
    private func loadCategories() {
        
        // create the categories
        let front = Category(image: UIImage(named: "frontImage"), name: "Front")
        let sports = Category(image: UIImage(named: "sportsImage"), name: "Sports")
        let wknd = Category(image: UIImage(named: "wkndImage"), name: "Weekend")
        let opinion = Category(image: UIImage(named: "opinionImage"), name: "Opinion")
        let university = Category(image: UIImage(named: "universityImage"), name: "University")
        let city = Category(image: UIImage(named: "cityImage"), name: "City")
        let scitech = Category(image: UIImage(named: "scitechImage"), name: "SciTech")
        let culture = Category(image: UIImage(named: "cultureImage"), name: "Culture")
        let ytv = Category(image: UIImage(named: "ytvImage"), name: "YTV")
        let mag = Category(image: UIImage(named: "magImage"), name: "Magazine")
        
        // add the categories to the array
        categories += [front, opinion, university, city, sports, scitech, culture, ytv, wknd, mag]
    }
}
