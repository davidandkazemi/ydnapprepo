//
//  SideBarTableViewCell.swift
//  YDN App
//
//  Created by David and Kazemi on 12/10/17.
//  Copyright © 2017 David Townley, Kazemi Adachi. All rights reserved.
//

import UIKit

class SideBarTableViewCell: UITableViewCell {

    // MARK: Properties
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var imageImageView: UIImageView!
    
    // configure the cell when it is given a specific category
    var category: Category! {
        didSet {
            categoryLabel.text = category.name
            imageImageView.image = category.image
        }
    }
}
