//
//  YTVViewController.swift
//  YDN App
//
//  Created by David and Kazemi on 12/10/17.
//  Copyright © 2017 David Townley, Kazemi Adachi. All rights reserved.
//

import UIKit
import WebKit
import os.log

class YTVViewController: UIViewController {
    
    // MARK: Properties
    @IBOutlet weak var wv: WKWebView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var backButton: UIBarButtonItem!
    
    var article: Article! = nil
    private var videoLink: String = ""
    
    let defaultSession = URLSession(configuration: .default)
    var dataTask: URLSessionDataTask?
    
    
    //MARK: Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // configure the view controller with attributes of the article
        if let article = article {
            titleLabel.text = article.title
            authorLabel.text = "by " + article.author.uppercased()
            dateLabel.text = formatDate(pubDate: article.pubDate)
            videoLink = article.videoLink
        }
        
        // load the youtube video
        loadYoutube(videoID: videoLink)
    }
    
    // load the youtube video
    func loadYoutube(videoID: String) {
        // check that the url string is properly converted to a url
        guard
            let youtubeURL = URL(string: videoID)
            else { return }
        // load into view
        wv.load( URLRequest(url: youtubeURL) )
    }
    
    // if the back button is pressed, dismiss the view controller
    @IBAction func back(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    // if the share button is clicked, open up the activity view controller
    @IBAction func shareButtonClicked(sender: UIButton) {
        let textToShare = self.article.title + ":"
        
        if let myWebsite = NSURL(string: self.article.link) {
            let objectsToShare = [textToShare, myWebsite] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            activityVC.popoverPresentationController?.sourceView = sender
            self.present(activityVC, animated: true, completion: nil)
        }
    }
}
