//
//  ArticleTableViewController.swift
//  YDN App
//
//  Created by David and Kazemi on 12/10/17.
//  Copyright © 2017 David Townley, Kazemi Adachi. All rights reserved.
//

import UIKit
import os.log
import RealmSwift

class ArticleTableViewController: UITableViewController, UINavigationControllerDelegate {
    
    // MARK: Properties
    
    // initializes global variables for the app
    private var articles: [Article]!
    private var recentlySelectedArticle: Article! = nil
    var currentCategory: Category!
    let searchController = UISearchController(searchResultsController: nil)
    var filteredArticles = [Article]()

    @IBOutlet weak var feedNavigationBar: UINavigationItem!
    
    
    // MARK: Action
    
    // if the hamburger icon is tapped, then send a notification to toggle the side menu
    @IBAction func onMoreTapped(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
    }
    
    // prepare the table view controller
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // prepare cells for dynamic resizing
        tableView.estimatedRowHeight = 155.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        // set navigation bar text to white
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        
        // in the case that the programmer is receiving Realm migration errors, the following code should be uncommented and run to clear the database
/*
        let config = Realm.Configuration(deleteRealmIfMigrationNeeded: true)
        Realm.Configuration.defaultConfiguration = config

        let realm = try! Realm()
*/
        
        // set initial category to the Front page, complete with the navigation bar image
        currentCategory = Category(image: UIImage(named: "frontImage"), name: "Front")
        addNavBarImage()
        
        // prepare the view controller to observe for notifications to change the category
        NotificationCenter.default.addObserver(self, selector: #selector(self.changeCategory(notification:)), name: NSNotification.Name("ChangeCategory"), object: nil)
        
        // prepare the view controller to observe for notifications to enable/disable user interaction on the feed view (when the side menu is out)
        NotificationCenter.default.addObserver(self, selector: #selector(toggleEnableMainView), name: NSNotification.Name("ToggleEnableMainView"), object: nil)
        
        // Do to outstanding bugs, the following should be left commented out unless the programmer wishes to upload more articles to the database

//        for source in [
//                    "https://yaledailynews.com/blog/category/opinion/feed",
//                    "https://yaledailynews.com/blog/category/university/feed/",
//                    "https://yaledailynews.com/blog/category/city/feed",
//                    "https://yaledailynews.com/blog/category/sports/feed",
//                    "https://yaledailynews.com/blog/category/sci-tech/feed/",
//                    "https://yaledailynews.com/blog/category/culture/feed",
//                    "https://yaledailynews.com/blog/category/ytv/feed",
//                    "https://yaledailynews.com/blog/category/wknd/feed/",
//                    "https://yaledailynews.com/blog/category/mag/feed"
//          ] {
//                        fetchData(source)
//        }
        

        
        // set up the search controller
        callSearch()
    }

    
    // gather data from the YDN website
    private func fetchData(_ url: String)
    {
        // create a parser
        let feedParser = FeedParser()
        
        // prepare to parse asynchronously so that the app only updates once the parser is done
        let group = DispatchGroup()
        group.enter()
        
        // feed the url into the parser
        feedParser.parseFeed(url: url) { (articles) in
            
            self.articles = articles

            let realm = try! Realm()
            
            // upload articles to the database
            for article in (self.articles)! {
                try! realm.write {
                    realm.add(article, update: true)
                }
            }
            group.leave()
        }
        group.wait()
    }
    
    
    // Handling the two different types of navigation, to articles, and to YTV videos
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //getting the index path of selected row
        let indexPath = tableView.indexPathForSelectedRow
        
        // access the selected cell
        guard let currentCell = tableView.cellForRow(at: indexPath!) as? ArticleTableViewCell else {
            fatalError("Unexpected cell type, not ArticleTableViewCell")
        }
        
        // save the cell's article
        recentlySelectedArticle = currentCell.item
        
        // initiate the proper segue for the given article type
        if currentCell.item.category == "YTV" {
            self.performSegue(withIdentifier: "ShowYTV", sender: self)
        }
        else {
            self.performSegue(withIdentifier: "ShowArticle", sender: self)
            os_log("ShowArticle")
        }
    }
    
    
    // MARK: - Navigation
    
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        switch(segue.identifier ?? "") {
        case "ShowArticle":
            guard let articleNavigationController = segue.destination as? UINavigationController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            
            // set the article property of the view controller to the selected article, and set the name of the back button to the current category being shown in the feed
            if let viewController = articleNavigationController.topViewController as! ArticleViewController? {
                viewController.article = recentlySelectedArticle
                viewController.backButton.title = self.currentCategory.name
            }
            
        case "ShowYTV":
            guard let articleNavigationController = segue.destination as? UINavigationController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            // do the same thing as before, however, we must now access the view controller as a YTVViewController
            if let viewController = articleNavigationController.topViewController as! YTVViewController? {
                viewController.article = recentlySelectedArticle
                viewController.backButton.title = self.currentCategory.name
            }
        default:
            fatalError("Unexpected Segue Identifier; \(String(describing: segue.identifier))")
        }
    }
    
    
    // MARK: - Table view data source
    
    
    // create just one section
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    // set the number of rows
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        // if the user is searching, return the same number of rows as matching articles
        if isFiltering() {
            return filteredArticles.count
        }
        // query realm database, and return as many rows as articles in the given category, sort by publication date
        else {
            let realm = try! Realm()
            let articles_0: Results<Article>
            if currentCategory.name == "Front" {
                articles_0 = realm.objects(Article.self).sorted(byKeyPath: "pubDate", ascending: false)
            } else {
                let query = "category == '" + currentCategory.name + "'"
                articles_0 = realm.objects(Article.self).filter(query).sorted(byKeyPath: "pubDate", ascending: false)
            }
            return articles_0.count
        }
    }

    // configure cells
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let realm = try! Realm()
        let articles_1: Results<Article>
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ArticleTableViewCell

        // if category is front, gather all articles
        if currentCategory.name == "Front" {
            articles_1 = realm.objects(Article.self).sorted(byKeyPath: "pubDate", ascending: false)
        }
        // otherwise gather all articles matching the requested category
        else {
            let query = "category == '" + currentCategory.name + "'"
            articles_1 = realm.objects(Article.self).filter(query).sorted(byKeyPath: "pubDate", ascending: false)
        }

        // add articles to article array
        articles = []
        for article in articles_1 {
            self.articles.append(article)
        }

        let item: Article

        // if the user is searching, return the ith filtered article for the ith cell
        if isFiltering() {
            item = filteredArticles[indexPath.item]
        }
        // otherwise, return the ith  article for the ith cell
        else {
            item = articles![indexPath.item]
        }

        // set the cell's item property to the given item
        cell.item = item
        
        return cell
    }
    
    // MARK: Private Methods
    
    // change the category of the feed
    @objc func changeCategory(notification: NSNotification) {
        // obtain the requested category from the notification and set it as the new category of the feed
        if let category = notification.userInfo?["category"] as? Category {
            currentCategory = category
            updateFeed()
            self.navigationItem.searchController?.isActive = false
        }
        // report an error if the category somehow didn't send
        else {
            fatalError("category did not transfer")
        }
    }
    
    // disable the user interaction on the feed when the side bar is out, and renable when side bar is hidden
    @objc private func toggleEnableMainView () {
        if self.view.isUserInteractionEnabled {
            self.view.isUserInteractionEnabled = false
        }
        else {
            self.view.isUserInteractionEnabled = true
        }
    }
    
    // update the feed when a new category is chosen
    private func updateFeed() {
        // reload the cells to show the new information
        OperationQueue.main.addOperation {
            self.tableView.reloadSections(IndexSet(integer: 0), with: .left)
        }
        
        // configure the navigation bar and search field for the proper category
        if currentCategory.name != "Front" {
            self.navigationItem.titleView = nil
            self.navigationItem.title = currentCategory.name
            searchController.searchBar.placeholder = "Search " + currentCategory.name
            print("change")
        } else {
            addNavBarImage()
            searchController.searchBar.placeholder = "Search All"
        }
        
        // scroll to the top of the feed
        scrollToTop()
        
    }
    
    
    // scroll to the first row (to the top)
    private func scrollToTop() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            let top = NSIndexPath(row: Foundation.NSNotFound, section: 0)
            self.tableView.scrollToRow(at: top as IndexPath, at: .top, animated: true)
        }
    }
    
    
    // add the image to the navigation bar when the category is Front
    private func addNavBarImage() {
        // create the image view and fill with YDN image
        // then configure proper settings
        let imageView = UIImageView(image: UIImage(named: "android-icon-192x192.png"))
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
        imageView.frame = titleView.bounds
        titleView.addSubview(imageView)
        // add to navigation bar
        self.navigationItem.titleView = titleView
    }
    
    // MARK: - Filtering methods
    
    // check if the search bar is empty
    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    // filter the array of articles for search words and scope
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        filteredArticles = articles!.filter({( article : Article) -> Bool in
            
            // check for matches in title, author, and content
            if scope == "All" {
                return article.title.lowercased().contains(searchText.lowercased()) || article.author.lowercased().contains(searchText.lowercased()) || article.content.lowercased().contains(searchText.lowercased())
            }
            // check just title
            else if scope == "Title" {
                return article.title.lowercased().contains(searchText.lowercased())
            }
            // check just author
            else if scope == "Author" {
                return article.author.lowercased().contains(searchText.lowercased())
            }
            // check just content
            else if scope == "Content" {
                return article.content.lowercased().contains(searchText.lowercased())
            }
            else {
                fatalError("unexpected scope: \(scope)")
            }
        })
        
        // reload the data in the table view to reflect the searched information
        
        tableView.reloadData()
    }
    
    // check to see if the user is filtering yet
    func isFiltering() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    // configure the search bar
    func callSearch() {
        // Setup the Search Controller https://www.raywenderlich.com/157864/uisearchcontroller-tutorial-getting-started
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search All"
        searchController.searchBar.barStyle = UIBarStyle.default
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue: UIColor.gray]
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        // Setup the Scope Bar (to select between different search categories)
        searchController.searchBar.scopeButtonTitles = ["All", "Title", "Author", "Content"]
        searchController.searchBar.delegate = self
        let cancelButtonAttributes: NSDictionary = [NSAttributedStringKey.foregroundColor: UIColor.white]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes as? [NSAttributedStringKey : Any], for: UIControlState.normal)
        // Selected text
        let titleTextAttributesSelected = [NSAttributedStringKey.foregroundColor: UIColor.white]
        UISegmentedControl.appearance().setTitleTextAttributes(titleTextAttributesSelected, for: .selected)
        // Normal text
        let titleTextAttributesNormal = [NSAttributedStringKey.foregroundColor: UIColor.white]
        UISegmentedControl.appearance().setTitleTextAttributes(titleTextAttributesNormal, for: .normal)
        searchController.searchBar.tintColor = UIColor.lightGray
    }
}


// filtering extensions for ArticleTableViewController class to give access to delegations over the search bar and search results
extension ArticleTableViewController: UISearchResultsUpdating {
    
    // MARK: - UISearchResultsUpdating Delegate
    
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        filterContentForSearchText(searchController.searchBar.text!, scope: scope)
    }
}


extension ArticleTableViewController: UISearchBarDelegate {
    
    // MARK: - UISearchBar Delegate
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
    }
}

